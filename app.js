/* eslint-disable require-jsdoc */
const express = require('express');
const morgan = require('morgan');
const config = require('config');
const cors = require('cors');

const connectToDb = require('./db/connectToDb');
const {authorization} = require('./middleware/authMiddleware');

const PORT = process.env.PORT || config.get('port');

const app = express();

app.use(cors());
app.use(express.json({extended: true}));
app.use(morgan('tiny'));

app.use('/api/auth', require('./routes/authRoutes'));

app.use(authorization);
app.use('/api/users', require('./routes/userRoutes'));
app.use('/api/trucks', require('./routes/trucksRoutes'));
app.use('/api/loads', require('./routes/loadsRoutes'));

app.use(require('./middleware/errorHandlerMiddleware'));

async function start() {
  try {
    await connectToDb();
    app.listen(PORT, () =>
      console.log(`Server has been started on port ${PORT}...`),
    );
  } catch (e) {
    console.log('Server error:', e.message);
  }
}

start();
