const mongoose = require('mongoose');
const config = require('config');

module.exports = async () => {
  try {
    await mongoose.connect(config.get('mongoUri'), {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
      useFindAndModify: false,
    });
  } catch (e) {
    console.log('Error connecting to database:', e.message);
  }
};
