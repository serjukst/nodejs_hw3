## NODEJS_HW3 - UBER like service for freight trucks

#### API documentation
Full API documentation can be found in `swagger.yaml`. 
You can past content into the editor window on **[Swagger](https://editor.swagger.io/)**.

#### Requirements
**[NodeJS](https://nodejs.org/en/)** have to installed on your machine.

#### For start application:
Run following commands in the terminal
1. **`npm install`**
2. **`npm run start`**
