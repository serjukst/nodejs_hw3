/* eslint-disable require-jsdoc */
class UserExistsError extends Error {
  constructor(messege) {
    super(messege);
    this.name = 'UserExistsError';
  }
}

class WrongPasswordError extends Error {
  constructor(messege) {
    super(messege);
    this.name = 'WrongPasswordError';
  }
}

class ForbiddenError extends Error {
  constructor(messege) {
    super(messege);
    this.name = 'ForbiddenError';
  }
}

class ExistsError extends Error {
  constructor(messege) {
    super(messege);
    this.name = 'ExistsError';
  }
}

class AuthError extends Error {
  constructor(messege) {
    super(messege);
    this.name = 'AuthError';
  }
}

class ValidationError extends Error {
  constructor(messege) {
    super(messege);
    this.name = 'ValidationError';
  }
}

module.exports = {
  UserExistsError,
  WrongPasswordError,
  ForbiddenError,
  ExistsError,
  AuthError,
  ValidationError,
};
