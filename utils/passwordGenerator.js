/* eslint-disable require-jsdoc */
const generator = require('generate-password');

class PasswordGenerator {
  generatePassword() {
    return generator.generate({
      length: 10,
      numbers: true,
    });
  }
}

module.exports = new PasswordGenerator();
