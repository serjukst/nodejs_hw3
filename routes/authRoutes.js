/* eslint-disable new-cap */
const {Router} = require('express');

const router = Router();

const validation = require('../middleware/requestValidationMiddleware');
const ctrl = require('../controllers/authController.js');

router.post('/forgot_password', validation.forgotPass, ctrl.forgotPassword);

router.post('/register', validation.register, ctrl.register);

router.post('/login', validation.login, ctrl.login);

module.exports = router;
