/* eslint-disable new-cap */
const {Router} = require('express');

const router = Router();

const ctrl = require('../controllers/truckController');
const validation = require('../middleware/requestValidationMiddleware');
const auth = require('../middleware/authMiddleware');

router.use(auth.accessForDriver);

router.route('/:id/assign').post(ctrl.assignTruck);

router
    .route('/:id')
    .get(ctrl.getTruckById)
    .put(validation.truckType, ctrl.updateTruck)
    .delete(ctrl.deleteTruck);

router.route('/').post(validation.truckType, ctrl.addTruck).get(ctrl.getTrucks);

module.exports = router;
