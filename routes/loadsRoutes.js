/* eslint-disable new-cap */
const {Router} = require('express');

const router = Router();

const ctrl = require('../controllers/loadsController');
const validation = require('../middleware/requestValidationMiddleware');
const auth = require('../middleware/authMiddleware');

router.route('/active/state').patch(auth.accessForDriver, ctrl.changeLoadState);

router.route('/active').get(auth.accessForDriver, ctrl.getActiveLoad);

router.route('/:id/post').post(auth.accessForShipper, ctrl.postLoad);

router
    .route('/:id/shipping_info')
    .get(auth.accessForShipper, ctrl.getShippingInfo);

router
    .route('/:id')
    .get(ctrl.getLoadById)
    .put(auth.accessForShipper, ctrl.updateLoad)
    .delete(auth.accessForShipper, ctrl.deleteLoad);

router
    .route('/')
    .post(auth.accessForShipper, validation.addLoad, ctrl.addLoad)
    .get(validation.getLoads, ctrl.getLoads);

module.exports = router;
