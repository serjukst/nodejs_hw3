/* eslint-disable new-cap */
const {Router} = require('express');

const router = Router();

const ctrl = require('../controllers/userController');
const validation = require('../middleware/requestValidationMiddleware');

router.route('/me/password').patch(validation.changePass, ctrl.changeUserPass);

router.route('/me').get(ctrl.getUser).delete(ctrl.deleteUser);

module.exports = router;
