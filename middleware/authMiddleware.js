const JwtService = require('../services/JwtService');
const jwtService = new JwtService();
const {errorsMsg} = require('../constants/messages');
const {driver, shipper} = require('../constants/userTypes');
const {AuthError} = require('../utils/customErrors');

module.exports.authorization = (req, res, next) => {
  if (req.method === 'OPTIONS') {
    return next();
  }

  try {
    const authHeader = req.headers['authorization'];

    if (!authHeader) {
      throw new AuthError(errorsMsg.noAuth);
    }

    const [, token] = authHeader.split(' ');

    const decoded = jwtService.verify(token);
    req.user = decoded;
    next();
  } catch (e) {
    next(new AuthError(e.message));
  }
};

module.exports.accessForDriver = (req, res, next) => {
  const {role} = req.user;
  if (role === driver) {
    next();
  } else {
    throw new AuthError(errorsMsg.accessOnlyForDriver);
  }
};

module.exports.accessForShipper = (req, res, next) => {
  const {role} = req.user;
  if (role === shipper) {
    next();
  } else {
    throw new AuthError(errorsMsg.accessOnlyForShipper);
  }
};
