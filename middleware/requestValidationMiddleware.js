const {ValidationError} = require('../utils/customErrors');

const schemas = require('../constants/joiSchemas');

const handleError = (error) => {
  if (error) {
    const message = error.details.map((i) => i.message);
    throw new ValidationError(message);
  }
};

module.exports.register = (req, res, next) => {
  const {error} = schemas.registerSchema.validate(req.body);
  handleError(error);

  next();
};

module.exports.login = (req, res, next) => {
  const {error} = schemas.loginSchema.validate(req.body);
  handleError(error);

  next();
};

module.exports.forgotPass = (req, res, next) => {
  const {error} = schemas.forgotPasswordSchema.validate(req.body);
  handleError(error);

  next();
};

module.exports.changePass = (req, res, next) => {
  const {error} = schemas.changePasswordSchema.validate(req.body);
  handleError(error);

  next();
};

module.exports.getLoads = (req, res, next) => {
  const {error} = schemas.GetLoadSchema.validate(req.query);
  handleError(error);

  next();
};

module.exports.addLoad = (req, res, next) => {
  const {error} = schemas.AddLoadSchema.validate(req.body);
  handleError(error);

  next();
};

module.exports.UpdateLoad = (req, res, next) => {
  const {error} = schemas.UpdateLoadSchema.validate(req.body);
  handleError(error);

  next();
};

module.exports.truckType = (req, res, next) => {
  const {error} = schemas.AddTruckSchema.validate(req.body);
  handleError(error);

  next();
};
