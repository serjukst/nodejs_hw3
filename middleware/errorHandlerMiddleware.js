const {errorsMsg} = require('../constants/messages');

module.exports = (err, req, res, next) => {
  let status = 500;
  let message = err.message;

  console.log(err.message);

  switch (err.name) {
    case 'UserExistsError':
    case 'WrongPasswordError':
    case 'ValidationError':
      status = 400;
      break;
    case 'AuthError':
      status = 401;
      break;
    case 'ForbiddenError':
      status = 403;
      break;
    case 'CastError':
    case 'ExistsError':
      status = 404;
      break;
    default:
      message = errorsMsg.smthWentWrong;
      break;
  }

  res.status(status).json({message});
};
