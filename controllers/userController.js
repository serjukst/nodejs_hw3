const User = require('../models/User');
const HashService = require('../services/HashService');
const UserService = require('../services/UserService');

const hashService = new HashService();
const userService = new UserService(hashService, User);

module.exports.getUser = async (req, res, next) => {
  try {
    const user = await userService.getUser(req.user.userId);
    res.json(user);
  } catch (e) {
    next(e);
  }
};

module.exports.deleteUser = async (req, res, next) => {
  try {
    const result = await userService.deleteUser(req.user.userId);
    res.json(result);
  } catch (e) {
    next(e);
  }
};

module.exports.changeUserPass = async (req, res, next) => {
  try {
    const result = await userService.changeUserPass(req.user.userId, req.body);
    res.json(result);
  } catch (e) {
    next(e);
  }
};
