const JwtService = require('../services/JwtService');
const HashService = require('../services/HashService');
const AuthService = require('../services/AuthService');
const User = require('../models/User');
const {usersMsg} = require('../constants/messages');

const jwtService = new JwtService();
const hashService = new HashService();
const authService = new AuthService({jwtService, hashService}, User);

module.exports.register = async (req, res, next) => {
  try {
    const user = await authService.register(req.body);
    res.status(201).json(user);
  } catch (e) {
    next(e);
  }
};

module.exports.login = async (req, res, next) => {
  try {
    const data = await authService.login(req.body);
    res.json(data);
  } catch (e) {
    next(e);
  }
};

module.exports.forgotPassword = async (req, res, next) => {
  try {
    await authService.forgotPassword(req.body.email);
    res.json({message: usersMsg.sendPassToEmail});
  } catch (e) {
    next(e);
  }
};

