const truckService = require('../services/TruckService');
const {trucksMsg} = require('../constants/messages');

module.exports.addTruck = async (req, res, next) => {
  try {
    const truck = await truckService.addTruck(req.body.type, req.user.userId);
    res.status(201).json({truck, message: trucksMsg.truckCreated});
  } catch (e) {
    next(e);
  }
};

module.exports.getTrucks = async (req, res, next) => {
  try {
    const trucks = await truckService.getTrucks(req.user.userId);
    res.json([{trucks}]);
  } catch (e) {
    next(e);
  }
};

module.exports.getTruckById = async (req, res, next) => {
  try {
    const truck = await truckService.getTruckById(
        req.params.id,
        req.user.userId,
    );
    res.json({truck});
  } catch (e) {
    next(e);
  }
};

module.exports.updateTruck = async (req, res, next) => {
  try {
    const truck = await truckService.updateTruck(
        req.params.id,
        req.body.type,
        req.user.userId,
    );
    res.json({truck, message: trucksMsg.truckUpdated});
  } catch (e) {
    next(e);
  }
};

module.exports.deleteTruck = async (req, res, next) => {
  try {
    await truckService.deleteTruck(req.params.id, req.user.userId);
    res.json({message: trucksMsg.truckDeleted});
  } catch (e) {
    next(e);
  }
};

module.exports.assignTruck = async (req, res, next) => {
  try {
    await truckService.assignTruck(req.params.id, req.user.userId);
    res.json({message: trucksMsg.truckAssigned});
  } catch (e) {
    next(e);
  }
};
