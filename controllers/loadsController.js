const loadsService = require('../services/LoadsService');
const {loadsMsg} = require('../constants/messages');

module.exports.addLoad = async (req, res, next) => {
  try {
    const load = await loadsService.addLoad(req.body, req.user.userId);
    res.status(201).json({load, message: loadsMsg.loadCreated});
  } catch (e) {
    next(e);
  }
};

module.exports.getLoads = async (req, res, next) => {
  try {
    const loads = await loadsService.getLoads(req.user.userId, req.query);
    res.json([{loads}]);
  } catch (e) {
    next(e);
  }
};

module.exports.getLoadById = async (req, res, next) => {
  try {
    const load = await loadsService.getLoadById(req.params.id, req.user.userId);
    res.json({load});
  } catch (e) {
    next(e);
  }
};

module.exports.getActiveLoad = async (req, res, next) => {
  try {
    const activeLoad = await loadsService.getActiveLoad(req.user.userId);
    res.json({activeLoad});
  } catch (e) {
    next(e);
  }
};

module.exports.changeLoadState = async (req, res, next) => {
  try {
    const load = await loadsService.changeLoadState(req.user);
    res.json({message: `Load state changed to '${load.state}'`});
  } catch (e) {
    next(e);
  }
};

module.exports.updateLoad = async (req, res, next) => {
  try {
    const newLoad = {...req.body, _id: req.params.id};
    await loadsService.updateLoad(newLoad, req.user.userId);
    res.json({message: loadsMsg.loadUpdated});
  } catch (e) {
    next(e);
  }
};

module.exports.deleteLoad = async (req, res, next) => {
  try {
    await loadsService.deleteLoad(req.params.id, req.user.userId);
    res.json({message: loadsMsg.loadDeleted});
  } catch (e) {
    next(e);
  }
};

module.exports.postLoad = async (req, res, next) => {
  try {
    const result = await loadsService.postLoad(req.params.id, req.user.userId);
    res.json({result});
  } catch (e) {
    next(e);
  }
};

module.exports.getShippingInfo = async (req, res, next) => {
  try {
    const result = await loadsService.getShippingInfo(
        req.params.id,
        req.user.userId,
    );
    res.json(result);
  } catch (e) {
    next(e);
  }
};
