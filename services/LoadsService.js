/* eslint-disable require-jsdoc */
const Load = require('../models/Load');
const truckService = require('../services/TruckService');
const {loadsMsg, trucksMsg} = require('../constants/messages');
const {NEW, POSTED, ASSIGNED, SHIPPED} = require('../constants/loadStatus');
const {ForbiddenError, ExistsError} = require('../utils/customErrors');

const {
  enRouteToPickUp,
  arrivedToPickUp,
  enRouteToDelivery,
  arrivedToDelivery,
} = require('../constants/loadState');

class LoadsService {
  constructor(Model) {
    this.Model = Model;
  }

  async addLoad(loadProps, userId) {
    const load = new this.Model({...loadProps, created_by: userId});
    await load.save();

    return load;
  }

  async getLoads(userId, query) {
    const {status = {$exists: true}, limit = 10, offset = 0} = query;
    const loads = await this.Model.find({
      $or: [{created_by: userId}, {assigned_to: userId}],
      status,
    })
        .limit(+limit)
        .skip(+offset)
        .select('-__v');

    if (!loads.length) {
      throw new ExistsError(loadsMsg.loadNotFind);
    }

    return loads;
  }

  async getLoadById(loadId, userId) {
    const load = await this.Model.findOne({
      _id: loadId,
      $or: [{created_by: userId}, {assigned_to: userId}],
    })
        .select('-__v')
        .exec();

    if (!load) {
      throw new ExistsError(loadsMsg.loadNotFind);
    }

    return load;
  }

  async getActiveLoad(userId) {
    const activeLoad = await this.Model.findOne({
      assigned_to: userId,
      status: {$nin: [NEW, SHIPPED]},
    })
        .select('-__v')
        .exec();

    if (!activeLoad) {
      throw new ExistsError(loadsMsg.loadNotFind);
    }

    return activeLoad;
  }

  async changeLoadState(user) {
    const activeLoad = await this.getActiveLoad(user.userId);
    const currentLoadState = activeLoad.state;

    switch (currentLoadState) {
      case enRouteToPickUp:
        activeLoad.state = arrivedToPickUp;
        break;
      case arrivedToPickUp:
        activeLoad.state = enRouteToDelivery;
        break;
      case enRouteToDelivery:
        activeLoad.state = arrivedToDelivery;
        activeLoad.status = SHIPPED;
        activeLoad.logs.push({
          message: `${loadsMsg.loadChangeStatus} ${activeLoad.status}`,
        });
        await truckService.changeTruckStatus(user.userId);
        break;
    }

    activeLoad.logs.push({
      message: `${loadsMsg.loadChangeState} ${activeLoad.state}`,
    });

    await activeLoad.save();

    return activeLoad;
  }

  async updateLoad(newLoad, userId) {
    const query = {_id: newLoad._id, created_by: userId};

    const load = await this.Model.findOne({...query}).exec();

    if (!load) {
      throw new ExistsError(loadsMsg.loadNotFind);
    }

    if (load.status !== NEW) {
      throw new ForbiddenError(loadsMsg.forbiddenChangeLoad);
    }

    await this.Model.updateOne({...query}, {...newLoad});
  }

  async deleteLoad(loadId, userId) {
    const load = await this.Model.findOne({
      _id: loadId,
      created_by: userId,
    }).exec();

    if (!load) {
      throw new ExistsError(loadsMsg.loadNotFind);
    }

    if (load.status !== NEW) {
      throw new ForbiddenError(loadsMsg.forbiddenDeleteLoad);
    }

    load.remove();
  }

  async postLoad(loadId, userId) {
    const load = await this.Model.findOne({
      _id: loadId,
      created_by: userId,
      status: NEW,
    }).exec();

    if (!load) {
      throw new ExistsError(loadsMsg.loadNotFind);
    }

    load.status = POSTED;

    load.logs.push({message: `${loadsMsg.loadChangeStatus} ${load.status}`});

    const truck = await truckService.findTruckForLoad(load);

    if (!truck) {
      load.status = NEW;
      load.logs.push({
        message:
          `${trucksMsg.truckNotFound}.` +
          `${loadsMsg.loadChangeStatus} ${load.status}`,
      });
      await load.save();
      return {driver_found: false, message: loadsMsg.loadNotPosted};
    }

    load.status = ASSIGNED;
    load.logs.push({
      message: `${loadsMsg.loadAssigned} ${truck.assigned_to}`,
    });

    load.assigned_to = truck.assigned_to;

    load.state = enRouteToPickUp;
    load.logs.push({message: `${loadsMsg.loadChangeState} ${load.state}`});
    await load.save();

    return {driver_found: true, message: loadsMsg.loadPosted};
  }

  async getShippingInfo(loadId, userId) {
    const activeLoad = await this.Model.findOne({
      _id: loadId,
      created_by: userId,
      status: {$nin: [NEW]},
    })
        .select('-__v')
        .exec();

    if (!activeLoad) {
      throw new ExistsError(loadsMsg.loadNotFind);
    }
    const truck = await truckService.getAssignedTruck(activeLoad.assigned_to);

    return {load: activeLoad, truck};
  }
}

module.exports = new LoadsService(Load);
