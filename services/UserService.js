/* eslint-disable camelcase */
/* eslint-disable require-jsdoc */
const {WrongPasswordError, UserExistsError} = require('../utils/customErrors');

const {usersMsg, errorsMsg} = require('../constants/messages');

class UserService {
  constructor(hashService, Model) {
    this.hashService = hashService;
    this.Model = Model;
  }

  async getUser(userId) {
    const user = await this.Model.findOne({_id: userId}).exec();

    if (!user) {
      throw new UserExistsError(errorsMsg.userNotExist);
    }

    const {_id, email, created_date, role} = user;

    return {_id, email, role, created_date};
  }

  async deleteUser(userId) {
    await this.Model.deleteOne({_id: userId}).exec();
    return {message: usersMsg.userDeleted};
  }

  async changeUserPass(userId, body) {
    const {oldPassword, newPassword} = body;
    const user = await this.Model.findOne({_id: userId}).exec();

    if (!user) {
      throw new UserExistsError(errorsMsg.userNotExist);
    }

    const isMatch = await this.hashService.compare(oldPassword, user.password);

    if (!isMatch) {
      throw new WrongPasswordError(errorsMsg.wrongPass);
    }

    user.password = await this.hashService.hash(newPassword, 12);
    await user.save();
    return {message: usersMsg.passwordChanged};
  }
}

module.exports = UserService;
