/* eslint-disable require-jsdoc */
const jwt = require('jsonwebtoken');
const config = require('config');

const secret = config.get('jwtSecret');
const signConfig = {expiresIn: '24h'};

class JwtService {
  sign(prop) {
    return jwt.sign(prop, secret, signConfig);
  }

  verify(token) {
    return jwt.verify(token, secret);
  }
}

module.exports = JwtService;
