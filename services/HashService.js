/* eslint-disable require-jsdoc */
const bcrypt = require('bcryptjs');

class HashService {
  async hash(password) {
    return await bcrypt.hash(password, 12);
  }

  async compare(password1, password2) {
    return await bcrypt.compare(password1, password2);
  }
}

module.exports = HashService;
