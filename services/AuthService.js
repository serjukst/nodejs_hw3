/* eslint-disable require-jsdoc */
const {
  UserExistsError,
  WrongPasswordError,
} = require('../utils/customErrors');

const {errorsMsg, usersMsg} = require('../constants/messages');
const sendMail = require('../utils/sendMail');
const {generatePassword} = require('../utils/passwordGenerator');

class AuthService {
  constructor(services, Model) {
    this.jwtService = services.jwtService;
    this.hashService = services.hashService;
    this.Model = Model;
  }

  async register(credentials) {
    const {email, password, role} = credentials;
    const candidate = await this.Model.findOne({email});

    if (candidate) {
      throw new UserExistsError(errorsMsg.userExist);
    }

    const hashedPassword = await this.hashService.hash(password);

    const user = new this.Model({email, password: hashedPassword, role});
    await user.save();

    const {_id} = user;

    return {_id, email, message: usersMsg.profileCreated};
  }

  async login(credentials) {
    const {email, password} = credentials;
    const user = await this.Model.findOne({email});

    if (!user) {
      throw new UserExistsError(errorsMsg.userNotExist);
    }

    const isMatch = await this.hashService.compare(password, user.password);

    if (!isMatch) {
      throw new WrongPasswordError(errorsMsg.wrongPass);
    }

    const token = this.jwtService.sign({
      userId: user.id,
      role: user.role,
      email,
    });

    return {jwt_token: token};
  }

  async forgotPassword(email) {
    const user = await this.Model.findOne({email});
    if (!user) {
      throw new UserExistsError(errorsMsg.userNotExist);
    }
    const newPassword = generatePassword();
    const hashedNewPassword = await this.hashService.hash(newPassword);
    user.password = hashedNewPassword;
    await sendMail(email, newPassword);
    await user.save();
  }
}

module.exports = AuthService;
