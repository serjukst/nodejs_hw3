/* eslint-disable require-jsdoc */
const Truck = require('../models/Truck');
const {ForbiddenError, ExistsError} = require('../utils/customErrors');
const {IS, OL} = require('../constants/truckStatus');
const truckTypes = require('../constants/truckTypes');
const {trucksMsg} = require('../constants/messages');

class TruckService {
  constructor(Model) {
    this.Model = Model;
  }

  async addTruck(type, userId) {
    const truck = new this.Model({type, created_by: userId});
    await truck.save();

    return truck;
  }

  async getTrucks(userId) {
    const trucks = await this.Model.find({created_by: userId}).select('-__v');

    if (!trucks.length) {
      throw new ExistsError(trucksMsg.truckNotFound);
    }

    return trucks;
  }

  async getTruckById(truckId, userId) {
    const truck = await this.Model.findOne({
      _id: truckId,
      created_by: userId,
    })
        .select('-__v')
        .exec();

    if (!truck) {
      throw new ExistsError(trucksMsg.truckNotFound);
    }

    return truck;
  }

  async updateTruck(truckId, newTruckType, userId) {
    const truck = await this.Model.findOne({
      _id: truckId,
      created_by: userId,
    }).exec();

    if (truck.assigned_to) {
      throw new ForbiddenError(trucksMsg.forbiddenChangeTruck);
    }

    truck.type = newTruckType;
    await truck.save();

    return truck;
  }

  async deleteTruck(truckId, userId) {
    const truck = await this.Model.findOne({
      _id: truckId,
      created_by: userId,
    }).exec();

    if (!truck) {
      throw new ExistsError(trucksMsg.truckNotFound);
    }

    if (truck.assigned_to) {
      throw new ForbiddenError(trucksMsg.forbiddenDeleteTruck);
    }

    await truck.remove();
  }

  async assignTruck(truckId, userId) {
    const truck = await this.Model.findOne({
      _id: truckId,
      created_by: userId,
    }).exec();

    if (!truck) {
      throw new ExistsError(trucksMsg.truckNotFound);
    }

    await this.Model.findOneAndUpdate(
        {
          assigned_to: userId,
        },
        {assigned_to: ''},
    ).exec();

    truck.assigned_to = userId;
    await truck.save();
  }

  async getAssignedTruck(userId) {
    const truck = await this.Model.findOne({
      assigned_to: userId,
    }).select('-__v').exec();

    if (!truck) {
      throw new ExistsError(trucksMsg.truckNotFound);
    }

    return truck;
  }

  async changeTruckStatus(userId) {
    const truck = await this.Model.findOne({assigned_to: userId}).exec();

    if (!truck) {
      throw new ExistsError(trucksMsg.truckNotFound);
    }

    truck.status = truck.status === IS ? OL : IS;
    await truck.save();
  }

  async findTruckForLoad(load) {
    const {
      payload,
      dimensions: {width, height, length},
    } = load;

    const trucks = await this.Model.find({
      assigned_to: {$nin: [null, '']},
      status: IS,
    });

    if (!trucks.length) {
      return null;
    }

    const acceptableTrucks = trucks.filter((truck) => {
      const [current] = truckTypes.filter((item) => item.type === truck.type);

      const res =
        current.payload >= payload &&
        current.width >= width &&
        current.height >= height &&
        current.length >= length;

      return res;
    });

    if (!acceptableTrucks.length) {
      return null;
    }

    const [truck] = acceptableTrucks;

    truck.status = OL;

    await truck.save();

    return truck;
  }
}

module.exports = new TruckService(Truck);
