const {Schema, model} = require('mongoose');

const {NEW, POSTED, ASSIGNED, SHIPPED} = require('../constants/loadStatus');
const state = require('../constants/loadState');

const schema = new Schema({
  created_by: {type: String, required: true},
  assigned_to: {type: String},
  status: {
    type: String,
    enum: [NEW, POSTED, ASSIGNED, SHIPPED],
    default: NEW,
  },
  state: {
    type: String,
    enum: [
      state.enRouteToPickUp,
      state.arrivedToPickUp,
      state.enRouteToDelivery,
      state.arrivedToDelivery,
    ],
  },
  name: {type: String, required: true},
  payload: {type: Number, required: true},
  pickup_address: {type: String, required: true},
  delivery_address: {type: String, required: true},
  dimensions: {
    width: {type: Number, required: true},
    length: {type: Number, required: true},
    height: {type: Number, required: true},
  },
  logs: [{message: String, time: {type: Date, default: Date.now}}],
  createdDate: {type: Date, default: Date.now},
});

module.exports = model('Load', schema);
