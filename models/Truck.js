const {Schema, model} = require('mongoose');

const {IS, OL} = require('../constants/truckStatus');

const [
  sprinter,
  smallStraight,
  largeStraight,
] = require('../constants/truckTypes');

const schema = new Schema({
  created_by: {type: String, required: true},
  assigned_to: {type: String},
  type: {
    type: String,
    required: true,
    enum: [sprinter.type, smallStraight.type, largeStraight.type],
  },
  status: {
    type: String,
    enum: [OL, IS],
    default: IS,
  },
  createdDate: {type: Date, default: Date.now},
});

module.exports = model('Truck', schema);
