const {Schema, model} = require('mongoose');
const {driver, shipper} = require('../constants/userTypes');

const schema = new Schema({
  email: {type: String, required: true, unique: true},
  password: {type: String, required: true},
  role: {
    type: String,
    enum: [driver, shipper],
    required: true,
  },
  created_date: {type: Date, default: Date.now},
});

module.exports = model('User', schema);
