module.exports.errorsMsg = {
  smthWentWrong: 'Something went wrong, try again...',
  wrongPass: 'Incorrect password, please try again',
  userNotExist: 'Such user not exist',
  userExist: 'Such user already exists',
  noAuth: 'No authorization',
  notFound: 'Not found',
  accessOnlyForDriver: 'Forbidden, access only for drivers',
  accessOnlyForShipper: 'Forbidden, access only for shipper',
};

module.exports.trucksMsg = {
  truckCreated: 'Truck created successfully',
  truckUpdated: 'Truck details changed successfully',
  truckDeleted: 'Truck deleted successfully',
  truckAssigned: 'Truck assigned successfully',
  truckNotFound: 'Truck not found',
  forbiddenChangeTruck: 'Forbidden change assigned truck',
  forbiddenDeleteTruck: 'Forbidden deleted assigned truck',
};

module.exports.loadsMsg = {
  loadCreated: 'Load created successfully',
  loadDeleted: 'Load deleted successfully',
  loadPosted: 'Load posted successfully',
  loadUpdated: 'Load details changed successfully',
  loadNotPosted: 'Load not posted',
  loadNotFind: 'Load not find',
  loadAssigned: 'Load assigned to driver with id',
  loadChangeState: 'Load change state to',
  loadChangeStatus: 'Load change status to',
  forbiddenChangeLoad: 'Forbidden change posted load',
  forbiddenDeleteLoad: 'Forbidden deleted posted load',
};

module.exports.usersMsg = {
  success: 'Success',
  profileCreated: 'Profile created successfully',
  sendPassToEmail: 'New password sent to your email address',
  passwordChanged: 'Password changed successfully',
  userDeleted: 'User deleted successfully',
};
