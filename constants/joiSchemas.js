const Joi = require('joi');
const {driver, shipper} = require('../constants/userTypes');
const {NEW, POSTED, ASSIGNED, SHIPPED} = require('../constants/loadStatus');
const [truck1, truck2, truck3] = require('../constants/truckTypes');

const email = Joi.string().email().required();
const password = Joi.string().required();
const role = Joi.string().valid(driver, shipper).required();
const type = Joi.string()
    .valid(truck1.type, truck2.type, truck3.type)
    .required();

module.exports.registerSchema = Joi.object({email, password, role});
module.exports.loginSchema = Joi.object({email, password});
module.exports.forgotPasswordSchema = Joi.object({email});
module.exports.changePasswordSchema = Joi.object({
  oldPassword: password,
  newPassword: password,
});

module.exports.GetLoadSchema = Joi.object({
  status: Joi.string().valid(NEW, POSTED, ASSIGNED, SHIPPED),
  limit: Joi.number().max(50),
  offset: Joi.number(),
});

module.exports.AddLoadSchema = Joi.object({
  name: Joi.string().required(),
  payload: Joi.number().required(),
  pickup_address: Joi.string().required(),
  delivery_address: Joi.string().required(),
  dimensions: Joi.object().keys({
    width: Joi.number().required(),
    length: Joi.number().required(),
    height: Joi.number().required(),
  }),
});

module.exports.UpdateLoadSchema = Joi.object({
  name: Joi.string(),
  payload: Joi.number(),
  pickup_address: Joi.string(),
  delivery_address: Joi.string(),
  dimensions: Joi.object().keys({
    width: Joi.number(),
    length: Joi.number(),
    height: Joi.number(),
  }),
});

module.exports.AddTruckSchema = Joi.object({type});
