module.exports = [
  {
    type: 'SPRINTER',
    width: 250,
    length: 300,
    height: 170,
    payload: 1700,
  },
  {
    type: 'SMALL STRAIGHT',
    width: 250,
    length: 500,
    height: 170,
    payload: 2500,
  },
  {
    type: 'LARGE STRAIGHT',
    width: 350,
    length: 700,
    height: 200,
    payload: 4000,
  },
];
